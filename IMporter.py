"""
    IMporter
    Copyright (C) 2021  Benjamin Kalloch, Ole Numssen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Contact: kalloch@cbs.mpg.de
"""

import sys
import xml.etree.ElementTree as elm_tree
import pathlib
import os
import time
import datetime
import shutil

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt


class Localization:
    WINDOW_TITLE_MAIN = "InstrumentMarker file importer"
    WINDOW_TITLE_PAT_LIST_FILE_SELECTOR = 'Select PatientList.xml file'
    WINDOW_TITLE_INSTR_MARK_FILE = 'Select InstrumentMarker file to import'

    SUBJECT_IDS_LBL = "Subject ID (Surname, Firstname)"
    SESSION_IDS_LBL = "Session ID (DD/MM/YYYY HH:MM:SS)"

    PATIENT_LIST_PREFIX = "Patient list file"
    INSTRUMENT_MARKER_PREFIX = "New InstrumentMarker file"

    INSERT_MARKER_BTN = "Import new InstrumentMarker File"

    MENU_FILE = "&File"
    MENU_EXIT = "&Exit"
    MENU_HELP = "&Help"
    MENU_LOAD_PAT_LIST = 'Load &Patient List'
    MENU_LOAD_INST_MARK_FILE = 'Select &InstrumentMarker File'
    MENU_INSTRUCTIONS = 'Instructions'
    MENU_ABOUT = 'About'

    TOOLTIP_EXIT = 'Exit application'
    TOOLTIP_PAT_LIST = 'Load patient list XML file'
    TOOLTIP_INST_MARK = 'Load new InstrumentMarker file'
    TOOLTIP_INVALID_FILE = 'Invalid file selected.'
    TOOLTIP_INSTRUCTIONS = 'Show usage instructions'
    TOOLTIP_ABOUT = 'Show licence'
    TOOLTIP_XML_NOT_WELL_FORMED = 'Selected XML file not well formed. Will not use this one.'

    LOADED = 'Loaded'
    HELP = 'Help'
    ABOUT = 'Licence'
    CONFIRMATION = 'Confirmation'
    IMPORTERROR = 'Import Error'

    HELPTEXT = f"" \
        f"<center>" \
        f"This tools imports an externally generated Instrument Marker file into a Localite TMSNavigator session." \
        f"</center> " \
        f"<ol>" \
        f"<li>Select the TMSNeuronavigator <code>PatientList.xml</code> file and " \
        f"the to be imported Instrument Marker file from the 'File' menu.</li> " \
        f"<li>Select the patient ID and the session ID in which the Instrument Markers should be imported.</li> " \
        f"<li>Hit '{INSERT_MARKER_BTN}' to imported the provided Instrument Marker File.</li>" \
        f"</ol>"

    ABOUTTEXT = f"" \
        "<center>IMporter, v.0.1<br>" \
        "Copyright (C) 2021  Benjamin Kalloch, Ole Numssen<br>" \
        "    <br>" \
        "This program is free software: you can redistribute it and/or modify<br>" \
        "it under the terms of the GNU General Public License as published by<br>" \
        "the Free Software Foundation, either version 3 of the License, or<br>" \
        "(at your option) any later version.<br>" \
        "    <br>" \
        "This program is distributed in the hope that it will be useful,<br>" \
        "but WITHOUT ANY WARRANTY; without even the implied warranty of<br>" \
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the<br>" \
        "GNU General Public License for more details.</center></br>"

    CONFIRMATIONTEXT = "Instrument Marker successfully imported."

    IMPORTERRORTEXT = "Import error. Please check file paths (session directory) and file permissions."


'''
 A custom QLabel class with a fixed text-prefix (must be at the time of instatiation)
 and a text that can be changed any time like in a standard QLabel.
 The background color of the label changes from red to green when the text property was set. 
'''


class PrefixedColoredLabel(QLabel):
    def __init__(self, prefix, parent=None):
        super().__init__(f"{prefix}: None", parent)

        self.prefix = prefix
        self.text = None

        self.label_style_text_missing = "background-color: #ff8080;"
        self.label_style_text_set = "background-color: #8cd98c;"

        self.setStyleSheet(self.label_style_text_missing)

    def setText(self, text):
        super().setText(f"{self.prefix}: {text}")
        self.setStyleSheet(self.label_style_text_set)
        self.text = text

    def reset(self):
        self.setStyleSheet(self.label_style_text_missing)
        self.text = None

    def textSet(self):
        return self.text is not None


class TextDialog(QDialog):
    def __init__(self, text, title):
        super().__init__()

        self.setWindowTitle(title)

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.accept)

        self.layout = QVBoxLayout()
        message = QLabel(text)
        self.layout.addWidget(message)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)


class Window(QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle(Localization.WINDOW_TITLE_MAIN)
        self.resize(640, 480)

        main_content = QWidget(self)
        list_content = QWidget(self)
        label_content = QWidget(self)
        toolbar_content = QWidget(self)

        self.setCentralWidget(main_content)

        # create GUI components
        self._createMenuBar()

        # init tooltips in toolbar
        self.statusBar().showMessage('')

        # labels
        self.subject_ids_LBL = QLabel(Localization.SUBJECT_IDS_LBL)
        self.session_ids_LBL = QLabel(Localization.SESSION_IDS_LBL)
        self.subject_ids_LBL.setAlignment(Qt.AlignCenter)
        self.session_ids_LBL.setAlignment(Qt.AlignCenter)

        # lists
        self.subjects_list = QListWidget()
        self.subjects_list.itemClicked.connect(self.load_sessions_cb)
        self.sessions_list = QListWidget()
        self.sessions_list.itemClicked.connect(self.check_and_enable_button)

        # toolbar
        self.patient_list_LBL = PrefixedColoredLabel(Localization.PATIENT_LIST_PREFIX)
        self.marker_pos_LBL = PrefixedColoredLabel(Localization.INSTRUMENT_MARKER_PREFIX)
        self.insert_marker_BT = QPushButton(Localization.INSERT_MARKER_BTN)
        self.insert_marker_BT.clicked.connect(self.insert_new_instrument_marker_file_cb)
        self.insert_marker_BT.setEnabled(False)

        # define layouts
        outer_layout = QVBoxLayout()
        outer_layout.addWidget(label_content)
        outer_layout.addWidget(list_content)
        outer_layout.addWidget(toolbar_content)

        label_layout = QHBoxLayout()
        label_layout.addWidget(self.subject_ids_LBL)
        label_layout.addWidget(self.session_ids_LBL)

        list_layout = QHBoxLayout()
        list_layout.addWidget(self.subjects_list)
        list_layout.addWidget(self.sessions_list)

        toolbar_layout = QVBoxLayout()
        toolbar_layout.addWidget(self.patient_list_LBL)
        toolbar_layout.addWidget(self.marker_pos_LBL)
        toolbar_layout.addWidget(self.insert_marker_BT)

        main_content.setLayout(outer_layout)
        list_content.setLayout(list_layout)
        label_content.setLayout(label_layout)
        toolbar_content.setLayout(toolbar_layout)

        self.patient_data_xml = None
        self.patient_list_xml = None
        self.marker_pos_file = None
        self.subjects = {}  # mapping from list entry names to subject folders
        self.sessions = {}  # ...same for sessions

        self.setWindowIcon(QIcon(os.path.join(os.path.dirname(os.path.realpath(__file__)), "icon.png")))

    def _createMenuBar(self):
        # create menu bar
        menu_bar = QMenuBar(self)
        self.setMenuBar(menu_bar)

        # add menu items
        file_menu = QMenu(Localization.MENU_FILE, self)
        help_menu = QMenu(Localization.MENU_HELP, self)

        # create actions for the menu items
        exit_act = QAction(Localization.MENU_EXIT, self)
        exit_act.setShortcut('Ctrl+Q')
        exit_act.setStatusTip(Localization.TOOLTIP_EXIT)
        exit_act.triggered.connect(qApp.quit)

        load_pat_list_act = QAction(Localization.MENU_LOAD_PAT_LIST, self)
        load_pat_list_act.setShortcut('Ctrl+P')
        load_pat_list_act.setStatusTip(Localization.TOOLTIP_PAT_LIST)
        load_pat_list_act.triggered.connect(self.load_subjects_cb)

        load_marker_file_act = QAction(Localization.MENU_LOAD_INST_MARK_FILE, self)
        load_marker_file_act.setShortcut('Ctrl+M')
        load_marker_file_act.setStatusTip(Localization.TOOLTIP_INST_MARK)
        load_marker_file_act.triggered.connect(self.load_marker_pos_cb)

        instructions_text_act = QAction(Localization.MENU_INSTRUCTIONS, self)
        instructions_text_act.setStatusTip(Localization.TOOLTIP_INSTRUCTIONS)
        instructions_text_act.triggered.connect(self.display_instructions_text_cb)

        about_text_act = QAction(Localization.MENU_ABOUT, self)
        about_text_act.setStatusTip(Localization.TOOLTIP_ABOUT)
        about_text_act.triggered.connect(self.display_about_text_cb)

        # add dropdown menus to menu items
        file_menu.addAction(load_pat_list_act)
        file_menu.addAction(load_marker_file_act)
        file_menu.addAction(exit_act)

        help_menu.addAction(about_text_act)
        help_menu.addAction(instructions_text_act)

        menu_bar.addMenu(file_menu)
        menu_bar.addMenu(help_menu)

    '''
     The button should only be enabled if a valid session of a subject was selected and
     both, the PatientList.xml as well as the new InstrumentMarker-file were loaded.
    '''

    def check_and_enable_button(self):
        if self.patient_list_LBL.textSet() \
                and \
                self.marker_pos_LBL.textSet() \
                and \
                self.sessions_list.currentItem() is not None:
            self.insert_marker_BT.setEnabled(True)
        else:
            self.insert_marker_BT.setEnabled(False)

    '''
     A callback that is called upon the button press:
     The loaded InstrumentMarker file is copied to the proper session directory.
    '''

    def insert_new_instrument_marker_file_cb(self):
        subject_id = self.subjects[self.subjects_list.currentItem().text()]
        session_id = self.sessions[self.sessions_list.currentItem().text()]

        if session_id is not None and subject_id is not None:
            # subject_id = subject_id.text()
            # session id in the format 'Sessions/Session_idstring/Session.xml'
            session_id = os.path.basename(
                os.path.dirname(
                    session_id
                )
            )
            base_dir = os.path.dirname(self.patient_data_xml)

            time_stamp = time.time()
            instrument_marker_fn = datetime.datetime.fromtimestamp(time_stamp).strftime(
                'InstrumentMarker%Y%m%d%H%M%S000.xml')

            session_target_path = os.path.join(base_dir, "Sessions", session_id)
            session_xml_path = os.path.join(session_target_path, "Session.xml")

            error_dialog = TextDialog(Localization.IMPORTERRORTEXT, Localization.IMPORTERROR)
            if os.access(session_target_path, os.R_OK) and os.access(session_xml_path, os.R_OK):
                instrument_marker_target_path = os.path.join(session_target_path, "InstrumentMarkers")
                if not os.path.exists(instrument_marker_target_path):
                    try:
                        os.makedirs(instrument_marker_target_path)
                    except:
                        error_dialog.exec()
                        return False
                try:
                    shutil.copy(self.marker_pos_file, os.path.join(instrument_marker_target_path, instrument_marker_fn))
                    self.replace_instrument_marker_id_in_session_xml(session_xml_path, instrument_marker_fn)
                    TextDialog(Localization.CONFIRMATIONTEXT, Localization.CONFIRMATION).exec()
                    return True
                except:
                    error_dialog.exec()
                    return False

            error_dialog.exec()
        return False

    '''
     A callback that spawns a QFileDialog to select the new InstrumentMarker file.
     (called from the corresponding action in the 'File' menu)
    '''

    def load_marker_pos_cb(self):
        home_dir = str(pathlib.Path.home())
        marker_pos_file = \
            QFileDialog.getOpenFileName(self, Localization.WINDOW_TITLE_INSTR_MARK_FILE, home_dir, 'XML files (*.xml)')[
                0]
        if os.path.exists(marker_pos_file):
            try:
                elm_tree.parse(marker_pos_file)  # validate the XML file
                self.marker_pos_file = marker_pos_file
                self.statusBar().showMessage(f"{Localization.LOADED} '{self.marker_pos_file}'")
                self.marker_pos_LBL.setText(self.marker_pos_file)
                self.check_and_enable_button()
            except elm_tree.ParseError as e:
                self.statusBar().showMessage(Localization.TOOLTIP_XML_NOT_WELL_FORMED)
        else:
            self.statusBar().showMessage(Localization.TOOLTIP_INVALID_FILE)

    '''
     A callback that spawns a QFileDialog to select a PatientList.xml file.
     (called from the corresponding action in the 'File' menu)     
    '''

    def load_subjects_cb(self):
        intital_path = r'C:\data-localite\Patients'
        intital_path = intital_path if os.path.exists(intital_path) else str(pathlib.Path.home())
        patient_list_xml = \
            QFileDialog.getOpenFileName(self, Localization.WINDOW_TITLE_PAT_LIST_FILE_SELECTOR, intital_path,
                                        'XML files (*.xml)')[0]
        try:
            self.patient_list_xml = patient_list_xml
            self.statusBar().showMessage(f"{Localization.LOADED} '{self.patient_list_xml}'")
            self.patient_ids_from_xml(self.patient_list_xml, self.subjects_list)
            self.patient_list_LBL.setText(self.patient_list_xml)
            self.check_and_enable_button()

        except Exception:
            self.statusBar().showMessage(Localization.TOOLTIP_INVALID_FILE)

    '''
     A callback to populate the sessions list of the GUI from the PatientData.xml file of a subject.
     (called upon clicking a subject in the subjects list of the GUI)
    '''

    def load_sessions_cb(self, data):
        base_dir = os.path.dirname(self.patient_list_xml)
        subject_id = self.subjects[data.text()]
        self.patient_data_xml = os.path.join(base_dir, subject_id, "PatientData.xml")

        try:
            self.sessions_from_xml(self.patient_data_xml, self.sessions_list)
        except Exception:
            pass

        # disable button when switching gbetween subjects as sessions must be selected
        self.check_and_enable_button()

    @staticmethod
    def display_instructions_text_cb(_):
        popup = TextDialog(Localization.HELPTEXT, Localization.HELP)
        popup.exec()

    @staticmethod
    def display_about_text_cb(_):
        popup = TextDialog(Localization.ABOUTTEXT, Localization.ABOUT)
        popup.exec()

    '''
     Load the patient IDs (given name, family name, ID) of the PatientList.xml
     from the respective PatientData.xml files.
     - Populates the 'subject list-view' of the GUI.
     - Populates the 'subjects' dictionary member.
    '''

    def patient_ids_from_xml(self, path, string_list):
        tree = elm_tree.parse(path)
        root = tree.getroot()

        string_list.clear()
        self.subjects = {}

        # Read all subjects from PatientList.xml
        patient_id_node_list = root.findall("./patientFolderReferenceList/LocFileReference/fileReferencePath")
        for patient_id_node in patient_id_node_list:
            patient_data_xml_fn = os.path.join(os.path.split(path)[0], patient_id_node.text, 'PatientData.xml')
            if not os.path.exists(patient_data_xml_fn):
                self.statusBar().showMessage(f"{patient_data_xml_fn} not found.")
                print(f"{patient_data_xml_fn} not found.")
                continue

            patient_data_root = elm_tree.parse(patient_data_xml_fn).getroot()
            name_fam, name_giv, name_id = '', '', ''
            name_fam_node = patient_data_root.find(".//familyName")
            name_giv_node = patient_data_root.find(".//givenName")
            name_id_node = patient_data_root.find(".//patientID")

            # The node might either be not present at all: "name_fam_node is None"
            # or it may not have any content: e.g. a single <familyName />-node
            if name_fam_node is not None:
                name_fam = '' if name_fam_node.text is None else name_fam_node.text
            if name_giv_node is not None:
                name_giv = '' if name_giv_node.text is None else name_giv_node.text
            if name_id_node is not None:
                name_id = '' if name_id_node.text is None else name_id_node.text

            string_list.addItem(f"{name_id} ({name_giv}, {name_fam})")
            self.subjects[f"{name_id} ({name_giv}, {name_fam})"] = patient_id_node.text

    '''
     Load the information of all sessions of a subject: name, date
     - Populates the 'session list-view' in the GUI.
     - Populates the 'session' dictionary member.
    '''

    def sessions_from_xml(self, path, string_list):
        tree = elm_tree.parse(path)
        root = tree.getroot()

        string_list.clear()
        self.sessions = {}

        # select all sessions of the PatientData.xml file
        sessionList_LocFileReference_fileReferencePath_list = root.findall(
            "./sessionList/LocFileReference/fileReferencePath")
        for session_path_node in sessionList_LocFileReference_fileReferencePath_list:
            ses_data_fn = os.path.join(os.path.split(path)[0], session_path_node.text)
            if not os.path.exists(ses_data_fn):
                self.statusBar().showMessage(f"{ses_data_fn} not found.")
                print(f"{ses_data_fn} not found.")
                continue
            session_root = elm_tree.parse(ses_data_fn).getroot()

            session_name_node = session_root.find("./name")
            session_date_node = session_root.find("./sessionDate")
            session_name, session_date = '', ''

            if session_name_node is not None:
                session_name = session_name_node.text
            if session_date_node is not None:
                t = datetime.datetime.strptime(session_date_node.text, "%Y%m%d%H%M%S.%f%z")
                session_date = t.astimezone(
                    datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo).strftime(
                    '%d/%m/%Y %H:%M:%S')

            string_list.addItem(f"{session_name} ({session_date})")
            self.sessions[f"{session_name} ({session_date})"] = session_path_node.text

    '''
     Open and edit the provided Session.xml file:
     - A new InstrumentMarker node is either created referencing the provided InstrumentMarker-file ID.
     - Or, if already resent, the node is modified to point to the provided InstrumentMarker-file ID.
    '''

    @staticmethod
    def replace_instrument_marker_id_in_session_xml(path_to_xml, instrument_marker_id):
        tree = elm_tree.parse(path_to_xml)
        root = tree.getroot()

        target_text = f"InstrumentMarkers/{instrument_marker_id}"

        # check whether an InstrumentMarkerList entry exisits in the session-XML
        query_result = root.find("./fileReferenceHashMap/entry/string[.='InstrumentMarkerList']")
        if query_result is None:
            fileReferenceHashMap = root.find("./fileReferenceHashMap")
            fileReferenceHashMap_entry = elm_tree.SubElement(fileReferenceHashMap, 'entry')
            fileReferenceHashMap_entry_string = elm_tree.SubElement(fileReferenceHashMap_entry, 'string')
            fileReferenceHashMap_entry_string.text = "InstrumentMarkerList"
            fileReferenceHashMap_entry_LocFileReference = elm_tree.SubElement(fileReferenceHashMap_entry,
                                                                              'LocFileReference')
            fileReferenceHashMap_entry_LocFileReference_fileReferencePath = elm_tree.SubElement(
                fileReferenceHashMap_entry_LocFileReference, 'fileReferencePath')
            fileReferenceHashMap_entry_LocFileReference_fileReferencePath.text = target_text
        else:  # xPath syntx: ... = parent, /// = all children, recursive
            fileReferencePath = root.find(
                "./fileReferenceHashMap/entry/string[.='InstrumentMarkerList']...//fileReferencePath")
            fileReferencePath.text = target_text

        tree.write(path_to_xml)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = Window()
    win.show()

    sys.exit(app.exec_())
