# IMporter

A small tool to import InstrumentMarker into a Localite TMS Navigator session.

## How to use
1. Copy `IMporter.exe` to the TMS Navigator PC and start it.
![01_startup](img/01_startup.png)

2. Select the TMS Navigator `PatientList.xml` file and the to be imported InstrumentMarker file from the *File* menu.
![02_load](img/02_load.png)
![03_import](img/03_import.png)

3. Select the `patient ID` and the `session ID` into which the InstrumentMarkers should be imported.

4. Hit `Import new InstrumentMarker File` to import the provided InstrumentMarker file.

## Caveats
This tools comes with no warranty.
